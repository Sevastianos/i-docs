import java.util.Date;

public class PerLogs {
	private Date dateOfView;
	private String movieTitle;
	private int minWtc;
	private int remainingMins;//the total minutes of the movie
	private String nameOfViewer;
	private String typeOfMovie;
	
	
	public String getTypeOfMovie() {
		return typeOfMovie;
	}
	public void setTypeOfMovie(String typeOfMovie) {
		this.typeOfMovie = typeOfMovie;
	}
	public String getNameOfViewer() {
		return nameOfViewer;
	}
	public void setNameOfViewer(String nameOfViewer) {
		this.nameOfViewer = nameOfViewer;
	}
	public Date getDateOfView() {
		return dateOfView;
	}
	public void setDateOfView(Date dateOfView) {
		this.dateOfView = dateOfView;
	}
	public String getMovieTitle() {
		return movieTitle;
	}
	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}
	public int getMinWtc() {
		return minWtc;
	}
	public void setMinWtc(int minWtc) {
		this.minWtc = minWtc;
	}
	public int getRemainingMins() {
		return remainingMins;
	}
	public void setRemainingMins(int remainingMins) {
		this.remainingMins = remainingMins;
	}
}
