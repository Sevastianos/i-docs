import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Workshop {

	public static void main(String[] args) throws IOException, ParseException {
		// TODO Auto-generated method stub
		
		Set<PerLogs> hashSet = new HashSet<PerLogs>();
		hashSet = parseAndCreateDataset(args[0],args[1],args[2]);
		
		
		//First Question Order List
		System.out.println("======Order List per watched Minutes======");
		ArrayList<String> orderList = new ArrayList<String>();
		orderList = orderListMovieByMinuteWatched(hashSet);
		
		
		System.out.println("======Average Per Movie======");
		//Second Question average per movie
		double averagePerMovie = 0;
		double totalAverage = 0;
		int count = 0;
		double finalMoviesAverage = 0;
		ArrayList<String> nameofMovieList = new ArrayList<String>();
		ArrayList<Integer> watchedMinuteList = new ArrayList<Integer>();
		ArrayList<Integer> totalMinuteList = new ArrayList<Integer>();
		Iterator<PerLogs> iterator = hashSet.iterator();
		while(iterator.hasNext()){
			PerLogs perLogs = iterator.next();
			nameofMovieList.add(perLogs.getMovieTitle());
			watchedMinuteList.add(perLogs.getMinWtc());
			totalMinuteList.add(perLogs.getRemainingMins());
		}
		
		for(int i=0; i<nameofMovieList.size(); i++){
			averagePerMovie = averagePerMovie(nameofMovieList.get(i),watchedMinuteList.get(i),totalMinuteList.get(i));
			totalAverage += averagePerMovie;
			count++;
			System.out.println("Title: "+ nameofMovieList.get(i)+ "  Average: "+averagePerMovie);
		}
		finalMoviesAverage = (double)totalAverage/count;
		System.out.println("Total Average: "+finalMoviesAverage);
	}
	
	
	//this is not a void method but return an array (the time is up :))
	public static void favoriteGenrePerPersonName(Set<PerLogs> hashSet){
		ArrayList<String> tmpPersonList = new ArrayList<String>();
		ArrayList<String> tmpTypeOfMovieList = new ArrayList<String>();
		ArrayList<Integer> tmpRemainingList = new ArrayList<Integer>();
		
		
		ArrayList<String> personList = new ArrayList<String>();
		ArrayList<String> typeOfMovieList = new ArrayList<String>();
		Iterator<PerLogs> iterator = hashSet.iterator();
		//the best type of movie is that that the remaining minutes in the minimum
		while(iterator.hasNext()){
			PerLogs perLogs = iterator.next();
			tmpPersonList.add(perLogs.getNameOfViewer());
			tmpTypeOfMovieList.add(perLogs.getTypeOfMovie());
			tmpRemainingList.add(perLogs.getRemainingMins()-perLogs.getMinWtc());
		}
		
		for(int i=0; i<tmpPersonList.size(); i++){
			if(tmpPersonList.get(i).equals("Person1")){
				//find the min per type of move
			}
			else if(tmpPersonList.get(i).equals("Person2")){
				//find the min per type of move
			}
			else{
				//find the min per type of move
			}
			//save in one table the type and the minimum remaining
		}
		//return the array;
	}
	
	
	public static double averagePerMovie(String nameOfMovie, int watched, int totalMinutes){
		double average;
		average = (watched*100)/ totalMinutes;
		return average;
	}
	
	public static ArrayList<String> orderListMovieByMinuteWatched(Set<PerLogs> hashSet){
		ArrayList<String> orderList = new ArrayList<String>();
		
		ArrayList<String> movieList = new ArrayList<String>();
		ArrayList<Integer> minuteWatchedList = new ArrayList<Integer>();
		
		Iterator<PerLogs> iterator = hashSet.iterator();
		while(iterator.hasNext()){
			PerLogs perLogs = iterator.next();
			movieList.add(perLogs.getMovieTitle());
			minuteWatchedList.add(perLogs.getMinWtc());
		}
		//call bubble sort
		orderList = bubbleSort(movieList,minuteWatchedList);
		
		for(String or : orderList){
			System.out.println(or);
		}
		
		
		return orderList;
	}
	
	
	public static Set<PerLogs> parseAndCreateDataset(String per1, String per2, String per3) throws IOException, ParseException{
		//save the input names into variables

		//Read the input txt files
		String linePer1;
		String linePer2;
		String linePer3;
		
		BufferedReader brPer1 = new BufferedReader(new FileReader(per1));
		BufferedReader brPer2 = new BufferedReader(new FileReader(per2));
		BufferedReader brPer3 = new BufferedReader(new FileReader(per3));
		
		//String regexString = "^(.*) (.*) (\\d+)min (\\d+)min (\\S+)$";
		String regexString = "^(\\d+)/(\\d+) (.*) (\\d+)min (\\d+)min (\\S+)$";		
		
		DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		Date date = new Date();
		
		//save the information into the hashset
		Set<PerLogs> hashSet = new HashSet<PerLogs>();
		while ((linePer1 = brPer1.readLine()) != null) {
				//for each line of the input files create an instance
				PerLogs perLogs = new PerLogs();
				Pattern pattern = Pattern.compile(regexString);
				Matcher matcher = pattern.matcher(linePer1);
				while(matcher.find()) {
					perLogs.setDateOfView(dateFormat.parse(matcher.group(1)));
					perLogs.setMinWtc(Integer.parseInt(matcher.group(5)));
					perLogs.setMovieTitle(matcher.group(3));
					perLogs.setNameOfViewer(per1.substring(0, per1.length()-4));
					perLogs.setTypeOfMovie(matcher.group(6));
					perLogs.setRemainingMins(Integer.parseInt(matcher.group(4)));
				   
					hashSet.add(perLogs);
			   }
		 }
		 while ((linePer2 = brPer2.readLine()) != null) {
			 	//for each line of the input files create an instance
			 	PerLogs perLogs = new PerLogs();
			
			 	//System.out.println(linePer1);
			 	Pattern pattern = Pattern.compile(regexString);
			 	Matcher matcher = pattern.matcher(linePer2);
			 	while(matcher.find()) {
			 		perLogs.setDateOfView(dateFormat.parse(matcher.group(1)));
			 		perLogs.setMinWtc(Integer.parseInt(matcher.group(5)));
			 		perLogs.setMovieTitle(matcher.group(3));
			 		perLogs.setNameOfViewer(per1.substring(0, per1.length()-4));
			 		perLogs.setTypeOfMovie(matcher.group(6));
			 		perLogs.setRemainingMins(Integer.parseInt(matcher.group(4)));
				   
			 		hashSet.add(perLogs);
			 	}
		   }
		   while ((linePer3 = brPer3.readLine()) != null) {
				//for each line of the input files create an instance
				PerLogs perLogs = new PerLogs();
				
				//System.out.println(linePer1);
				Pattern pattern = Pattern.compile(regexString);
				Matcher matcher = pattern.matcher(linePer3);
				while(matcher.find()) {
					perLogs.setDateOfView(dateFormat.parse(matcher.group(1)));
					perLogs.setMinWtc(Integer.parseInt(matcher.group(5)));
					perLogs.setMovieTitle(matcher.group(3));
					perLogs.setNameOfViewer(per1.substring(0, per1.length()-4));
					perLogs.setTypeOfMovie(matcher.group(6));
					perLogs.setRemainingMins(Integer.parseInt(matcher.group(4)));
				   
					hashSet.add(perLogs);
				}
		   }
		   return hashSet;
	  }
	
	public static ArrayList<String>  bubbleSort(ArrayList<String> arrMovieTitle, ArrayList<Integer> arrMinuteWatched) {  
        int n = arrMinuteWatched.size();  
        int temp = 0;  
        String tempMT = null;
         for(int i=0; i < n; i++){  
                 for(int j=1; j < (n-i); j++){  
                          if(arrMinuteWatched.get(j-1)>arrMinuteWatched.get(j)){  
                                 //swap elements  
                                 temp = arrMinuteWatched.get(j-1);  
                                 arrMinuteWatched.set(j-1, arrMinuteWatched.get(j));
                                 arrMinuteWatched.set(j, temp);

                                 tempMT = arrMovieTitle.get(j-1);  
                                 arrMovieTitle.set(j-1, arrMovieTitle.get(j));
                                 arrMovieTitle.set(j, tempMT);
                         }  
                          
                 } 
         }  
         return arrMovieTitle;
	}
}
